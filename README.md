# sisop-praktikum-modul-3-2023-HS-E06


## Praktikum Modul-3 Sistem Operasi


- 5025211049	Zakia Kolbi
- 5025211210	Nadiah Nuri Aisyah
- 5025211216	Akmal Nafis

# Nomor 1
# Nomor 2
a. Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.
```c
    int matriks1[4][2];
    int matriks2[2][5];
    int hasil[4][5] = {0};
    srand(time(NULL));
```
- Mendefinisikan matriks pertama, kedua, dan hasil serta srand untuk seed random generator.
```c
int i, j, k;
    //inisialisasi matriks 1
    for(i = 0; i<4; i++){
        for(j = 0; j<2; j++){
            matriks1[i][j] = rand()%5+1; //diisi angka random 1-5
        }
    }

    //inisialisasi matriks 2
    for(i=0; i<2; i++){
        for(j=0; j<5; j++){
            matriks2[i][j] = rand()%4+1; //diisi angka random 1-4
        }
    }
```
- Inisialisasi matriks 1 dan 2 menggunakan fungsi rand untuk mendapatkan angka random dalam rentang
```c
//mengalikan matriks
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            for(k=0; k<2; k++){
                hasil[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }
```
- Mengalikan matriks dan di store di hasil
```c
//menampilkan hasil
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }
```
- Menampilkan hasil perkalian matriks
```c
// membuat segment shared memory
    if ((shmid = shmget(key, sizeof(int), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    // menambahkan ke segmen shared memory
    if ((matrix = shmat(shmid, NULL, 0)) == (int (*)[5]) -1) {
        perror("shmat");
        exit(1);
    }

    // menyalin hasil ke shared memory
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            matrix[i][j] = hasil[i][j];
        }
    }

    // melepaskan dari shared memory
    shmdt(matrix);
```
- Menambahkan hasil perkalian matriks ke shared memory

b. Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)
```c
// mendapatkan segmen memori
    if ((shmid = shmget(key, sizeof(int), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    if ((matrix = shmat(shmid, NULL, 0)) == (int (*)[5]) -1) {
        perror("shmat");
        exit(1);
    }

    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            hasil[i][j] = matrix[i][j];
        }
    }

    shmdt(matrix);
```
- Mengambil matriks sebelumnya dari segmen memori
```c
printf("matriks perkalian:\n");
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }
```
- Menampilkan matriks

c. Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.
Contoh: 
array [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], ...],

maka:

1 2 6 24 120 720 ... ... …
(Catatan: Wajib menerapkan thread dan multithreading dalam penghitungan faktorial)
```c
void *faktorial(void *arg){
    unsigned long long res = 1;
    int n = *(int*)arg;
    int i;
    for(i=1; i<=n; i++){
        res *= i;
    }
    pthread_exit((void*)res);
}
```
- Fungsi untuk menghitung faktorial
```c
pthread_t threads[4][5];
    int thread_args[4][5];

    // membuat thread untuk menghitung faktorial tiap elemen
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            thread_args[i][j] = hasil[i][j];
            pthread_create(&threads[i][j], NULL, faktorial, &thread_args[i][j]);
        }
    }

    // menggabungkan thread dan disimpan di matrix baru
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            pthread_join(threads[i][j], (void**)&temp);
            fakt[i][j] = temp;
        }
    }
```
- Menghitung faktorial dengan menggunakan thread. Setiap elemen akan ditangani oleh sebuah thread. Setelah perhitungan selesai, thread digabungkan dengan pthread_join.
```c
    printf("faktorial matriks:\n");
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            printf("%llu ", fakt[i][j]);
        }
        printf("\n");
    }
```
- Menampilkan faktorial matriks
```c
clock_t start = clock();
```
- Memulai hitungan waktu eksekusi
```c
clock_t end = clock();
```
- Mengakhiri perhitungan waktu eksekusi
```c
double waktu = ((double)(end - start)) / CLOCKS_PER_SEC;
    printf("Waktu eksekusi program : %f detik\n", waktu);
```
- Menampilkan waktu eksekusi program

d. Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak.
```c
unsigned long long faktorial(int n){ 
    unsigned long long res = 1;
    int i;
    for(i=1; i<=n; i++){
        res *= i;
    }
    return res;
}
```
- Fungsi faktorial yang digunakan di sisop.c 
```c
for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            temp = hasil[i][j];
            fakt[i][j] = faktorial(temp);
        }
    }
}
```
- Menghitung semua elemen dengan fungsi
```c
clock_t start = clock();
```
- Memulai hitungan waktu eksekusi
```c
clock_t end = clock();
```
- Mengakhiri perhitungan waktu eksekusi
```c
double waktu = ((double)(end - start)) / CLOCKS_PER_SEC;
    printf("Waktu eksekusi program : %f detik\n", waktu);
```
- Menampilkan waktu eksekusi program
- Setelah cinta.c dan sisop.c dijalankan, program cinta.c yang menggunakan thread memerlukan waktu eksekusi yang lebih lama daripada sisop.c yang tidak menggunakan thread.

# Nomor 3
- membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.


user.c

```bash
struct mesg_buffer {
    long mesg_type;
    char mesg_text[MAX];
} message;
```
inisiasi variabel dengan tipe mesg_buffer dengan anggota message.mesg_type dan message.mesg_text untuk menyimpan pesan sebuah perintah

```bash
int main()
{
    key_t key;
    int msgid;
    pid_t pid = getpid();

    // ftok to generate unique key
    key = ftok("progfile", 65);

    // msgget creates a message queue
    // and returns identifier
    msgid = msgget(key, 0666 | IPC_CREAT);
```
deklarasikan beberapa variabel, yaitu key, msgid, dan pid. ftok() untuk menghasilkan kunci dengan nilai 65

msgget() untuk membuat sebuah antrian pesan dengan menggunakan kunci yang dihasilkan sebelumnya, dengan mode akses 0666. IPC_CREAT membuat antrian pesan .

```bash
while(1){
    // generate a message type based on the process ID
    message.mesg_type = pid;

    printf("USER PID: %d\n", pid);
    printf("Enter message: ");
    fgets(message.mesg_text, MAX, stdin);

    // msgsnd to send message
    msgsnd(msgid, &message, sizeof(message), 0);

    // display the message
    printf("Data sent is : %s\n", message.mesg_text);

    if (strcmp(message.mesg_text, "END\n") == 0) {
            break;
    }
}
    return 0;
    ```
pid digunakan sebagai user id, dalam menjalankan proses, fgets untuk menerima perintah dari user, msgsnd() digunakan untuk mengirim pesan dengan sistem antrian, program akan mengirim pesan yang terdapat pada variabel message ke antrian pesan yang ditentukan oleh msgid.

stream.c 


untuk mendownload library jansson  `sudo apt-get install -y libjansson-dev`

header yang digunakan
- sys/ipc.h untuk tipe data key_t dan message queue
- jansson.h untuk library yang digunakan untuk membaca dan menulis JSON.
- ctype.h untuk fungsi-fungsi pemrosesan karakter.
- semaphore.h untuk fungsi-fungsi semafor.
- string.h untuk fungsi-fungsi manipulasi string.
- string.h untuk fungsi-fungsi manipulasi string.
- ctype.h untuk fungsi-fungsi pemrosesan karakter.

```bash 
#define MAX_SENDERS 2 
#define MAX_MSG_SIZE 240

sem_t mutex;

// structure for message queue
struct mesg_buffer {
    long mesg_type;
    char mesg_text[MAX_MSG_SIZE];

} message;
```
`MAX_SENDERS` untuk menampung jumlah maksimum, user pengirim pesan. `MAX_MSG_SIZE` menentukan ukuran maksimum pesan yang dapat dikirim oleh pengirim dan diterima oleh penerima. sem_t mutex untuk mengatur akses dari user.

```bash
void download_file() {
    pid_t child_id;
    int status;
    char *url = "https://transfer.sh/get/tN3N7I/song-playlist.json";
    char *filename = "song-playlist.json";
    if((child_id = fork()) == 0){
    execlp("wget", "wget", "--no-check-certificate", url, "-O", filename, NULL);
    }
    while(wait(&status) > 0);
    printf("file downloaded\n");
   
}
```

```bash

void decode_rot13(const char* str, char* result) {
    int i;
    for (i = 0; str[i] != '\0'; ++i) {
        char c = str[i];
        if (c >= 'a' && c <= 'z') {
            result[i] = (((c - 'a') + 13) % 26) + 'a';
        } 
        
        else if (c >= 'A' && c <= 'Z') {
            result[i] = (((c - 'A') + 13) % 26) + 'A';
        } 
        
        else {
            result[i] = c;
        }
    }
    result[i] = '\0';
}

void decode_base64(const char* encrypt, char* decrypt) {
    const char b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    char b[4];
    int i = 0;
    int j = 0;
    
    while (encrypt[i]) {
        
        if (isspace(encrypt[i])) {
            i++;
            continue;
        }
        int k = 0;
        for (k = 0; k < 4 && encrypt[i]; k++, i++) {
            const char* pos = strchr(b64chars, encrypt[i]);
            if (pos == NULL) {
                decrypt[j] = '\0';
                return;
            }
            b[k] = pos - b64chars;
        }

        decrypt[j++] = (b[0] << 2) | (b[1] >> 4);

        if (k > 2) {
            decrypt[j++] = (b[1] << 4) | (b[2] >> 2);
        }
        if (k > 3) {
            decrypt[j++] = (b[2] << 6) | b[3];
        }
    }
    
    decrypt[j] = '\0';
}



void decode_hex(const char* input, char* output) {
    size_t len = strlen(input);
    if (len % 2 != 0) {
        // Invalid input length
        return;
    }

    size_t output_len = len / 2;
    char temp[3];
    temp[2] = '\0';

    for (size_t i = 0; i < output_len; i++) {
        temp[0] = tolower(input[i * 2]);
        temp[1] = tolower(input[i * 2 + 1]);

        int value = 0;
        for (size_t j = 0; j < 2; j++) {
            if (temp[j] >= '0' && temp[j] <= '9') {
                value = (value << 4) + (temp[j] - '0');
            } else if (temp[j] >= 'a' && temp[j] <= 'f') {
                value = (value << 4) + (temp[j] - 'a' + 10);
            } else {
                // Invalid input character
                return;
            }
        }
        output[i] = (char) value;
    }
    output[output_len] = '\0';
}
```

download_file() digunakan untuk mendownload file json yang tersedia, yang nantinya bernama "song-playlist.json"

fungsi yang digunakan untuk mendecode string dari file .json
void decode_rot13() 
void decode_base64()
void decode_hex()

```bash
key_t key;
    int msgid, i;
    char senders[MAX_SENDERS][MAX_MSG_SIZE] = {0};
    int sender_count = 0;
    char file_name[] = "song-playlist.json";
    char file_name_output[] = "playlist.txt";
    

    // ftok to generate unique key
    key = ftok("progfile", 65);

    // msgget creates a message queue
    // and returns identifier
    msgid = msgget(key, 0666 | IPC_CREAT);

    sem_init(&mutex, 0, 2);
```
msqid merupakan variabel message queue yyang diterima dari user, sem_init untuk mengatur thread yang digunakan.

```bash
sem_post(&mutex);
        // check for messages
        if (msgrcv(msgid, &message, sizeof(message), 0, IPC_NOWAIT) != -1) {

            // check if sender is already registered
            int found = 0;
            for (i = 0; i < sender_count; i++) {
                if (message.mesg_type == atol(senders[i])) {
                    found = 1;
                    # printf("system overload\n");
                    break;
                }
            }
 
            // if not, add sender to list
            if (!found) {
                sprintf(senders[sender_count], "%ld", message.mesg_type);
                sender_count++;
            }

        
         sem_post(&mutex);

    printf("\nReceived message %s from user %ld\n\n", message.mesg_text, message.mesg_type);
    
    ```

-  Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.

`sem_post(&mutex)` digunakan untuk membebaskan sebuah proses, untuk mendapatkan akses.
`msgrcv()` merupakan fungsi untuk menerima pesan dari message queue yang memiliki identifier msgid.
sender disini merupakan user yang mengirim pesan, jika sender yang mengirim pesan lebih dari batas maka system akan overload

```bash
    if (strstr(message.mesg_text, "PLAY") == NULL && 
    strstr(message.mesg_text, "BREAK") == NULL && 
    strstr(message.mesg_text, "LIST") == NULL && 
    strstr(message.mesg_text, "ADD") == NULL) {
    printf("UNKNOWN COMMAND\n");
    }
```
- Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

Sebuah kondisi agar User mengirimkan pesan perintah yang sesuai dengan fungsi strstr agar mengecek string yang diterima sesuai dengan perintah yang ada

```bash
if (strstr(message.mesg_text, "DECRYPT") != NULL){
            
                printf("Decrypting...\n");
                download_file();

....

                FILE *fp = fopen(file_name, "rb");
                if (!input_file) {
                        printf("Error: cannot open  %s\n", file_name);
                    }
                // printf("opening json done file\n");
```

- Mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json
Jika stream.c menerima pesan "DECRYPT" maka program akan mendownload dan mengdencode string yang ada didalam json, file dibuka dengan mode "rb" (read binary) menggunakan fungsi fopen

```bash
                fseek(fp, 0L, SEEK_END);
                long fileSize = ftell(fp);
                rewind(fp);

                // printf("read\n");

                char* buffer = (char*)calloc(fileSize + 1, sizeof(char));

                // printf("read content\n");
                size_t bytesRead = fread(buffer, 1, fileSize, fp);
                buffer[bytesRead] = '\0';
                fclose(fp);
                json_error_t error;

                json_t *root = json_loads(buffer, 0, &error);
                free(buffer);
```

untuk mengalokasikan memori dalam membaca file .json digunakan penghitungan ukuran file menggunakan fungsi `fseek()`,  dengan argumen `SEEK_END` untuk menentukan posisi pointer pada akhir file, fungsi `calloc()` untuk menyimpan isi file yang akan dibaca. 
menggunakan library jansson, isi file yang berformat JSON diubah menjadi objek JSON menggunakan fungsi `json_loads()`. `free(buffer)` digunakan untuk membebaskan alokasi memori yang dibuat tadi.



```bash


 fp = fopen(file_name_output, "w");
                if (!output_file) {
                        printf("Error: cannot open  %s\n", file_name_output);
                }

                size_t index;
                json_t *value;

                json_array_foreach(root, index, value) {
                    
                    json_t *method = json_object_get(value, "method");
                    json_t *song = json_object_get(value, "song");

                    const char *methodstring = json_string_value(method);
                    const char *songstring = json_string_value(song);

                    // Decrypt json file
                    char file_decrypt[MAX_MSG_SIZE];
                    
                if (strcmp(methodstring, "hex") == 0) {
                        if(songstring != NULL){
                            decode_hex(songstring, file_decrypt);
                            fprintf(fp, "%s\n", file_decrypt);
                        }                    
                    } else if (strcmp(methodstring, "base64") == 0) {
                        if(songstring != NULL){
                            decode_base64(songstring, file_decrypt);
                            fprintf(fp, "%s\n", file_decrypt);
                        }
                    } else if (strcmp(methodstring, "rot13") == 0) {
                        if(songstring != NULL){
                            decode_rot13(songstring, file_decrypt);
                            fprintf(fp, "%s\n", file_decrypt);
                        }  
                    } else {
                        printf("method is wrong %s\n", methodstring);
                        continue; // Skip the current song and continue with the next song.
                    }
               
                }

                fclose(fp);

                json_decref(root);


                system("sort  playlist.txt > sorted_playlist.txt");
                system("sort  sorted_playlist.txt > playlist.txt ");
                system("rm sorted_playlist.txt");
                }
                
```
- `json_array_foreac`h digunakan untuk mengiterasi setiap elemen pada array json alam array JSON yang disimpan di dalam variabel root. Setiap elemen akan diproses di dalam blok kode yang ada di antara kurung kurawal.
- `json_t *method`  Fungsi ini digunakan untuk mengambil nilai dari properti "method" dalam objek JSON value dan menyimpannya ke dalam variabel method.
- `json_t *song ` Fungsi ini digunakan untuk mengambil nilai dari properti "song" dalam objek JSON value dan menyimpannya ke dalam variabel song.
- `const char *methodstring`  Fungsi ini digunakan untuk mengambil nilai string dari variabel method yang sebelumnya telah diperoleh dari JSON, dan menyimpannya ke dalam variabel methodstring.
- `const char *songstring = json_string_value(song)` Fungsi ini digunakan untuk mengambil nilai string dari variabel song yang sebelumnya telah diperoleh dari JSON, dan menyimpannya ke dalam variabel songstring.

- ketika sudah mendecrypt file json maka isi file playlist.txt akan di sort menggunakan `system("rm sorted_playlist.txt");`

```bash
if (strstr(message.mesg_text, "LIST") != NULL){
            
                system("cat playlist.txt | sort ");
               // break;
        }
```
- Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt

Perintah list menggunakan fungsi System untuk menampilkan isi file dengan perintah `cat`. 

```bash

if (strstr(message.mesg_text, "PLAY") != NULL){

                char delim[] = "\"";
                char *token;

                token = strtok(message.mesg_text, delim);

                // cari 
                token = strtok(NULL, delim);

            char line[1000];
            int count = 0;

            FILE *fp = fopen(file_name_output, "r");

            while (fgets(line, 1000, fp) != NULL) {
                if (strcasestr(line, token) != NULL) {
                count++;
                }
            }

            if (count == 1) {
            printf("USER %ld PLAYING \"%s\" \n", message.mesg_type, token);

            } else if (count > 1) {
                
                printf("THERE ARE %d SONG CONTAINING  \"%s\" \n",count, token );
            
                fclose(fp);
                count = 0;
                fp = fopen(file_name_output, "r");


                while (fgets(line, 100, fp) != NULL) {
                if (strcasestr(line, token) != NULL) {
                    count ++;

                printf("%d. %s", count, line);
                }
            }
            } else {
                printf("  THERE IS NO SONG CONTAINING \"%s\" \n", token);
            }

            fclose(fp);

            

            }

```
- User juga dapat mengirimkan perintah PLAY <SONG> dengan ketentuan sebagai berikut.

Dalam perintah PLAY program akan menggunakan  `delim[] = "\""` untuk memisahkan antara command dan judul lagu yang diberikan, kemudian string lagu tersebut akan disimpan didalam token.
Fungsi `strtok()` digunakan untuk memecah sebuah string (dalam hal ini message.mesg_text) menjadi beberapa token berdasarkan sebuah delimiter , yaitu tanda "
Fungsi `fgets()` digunakan untuk membaca setiap baris dalam file yang telah dibuka di dalam variabel,` fgets` akan membaca setiap baris atau line pada file, kemudian mengembalikan nilai NULL jika selesai
agar pencarian tidak case sensitive maka digunakan `strcasestr` agar tidak terjadi eror dalam penggunaannya amak digunakan `#define _GNU_SOURCE`

```bash
if (strstr(message.mesg_text, "ADD") != NULL){
            
                char delim[] = "\"";
                char *token;

                token = strtok(message.mesg_text, delim);

                // cari 
                token = strtok(NULL, delim);

            char line[1000];
            int countAdd = 0;

            FILE *fp = fopen(file_name_output, "r");

            while (fgets(line, 1000, fp) != NULL) {
                if (strcasestr(line, token) != NULL) {
                countAdd++;
                }
                
            }

             fclose(fp);

            if (countAdd == 0 ) {
                FILE *fp = fopen(file_name_output, "a");
                fprintf(fp, "%s\n", token);
                fclose(fp);

                printf("USER %ld ADD \"%s\" \n", message.mesg_type, token);
            } else if (countAdd > 0){
                printf("SONG ALREADY ON PLAYLIST\n");
            }

        }
```

- User juga dapat menambahkan lagu ke dalam playlist 
untuk perintah ADD, mirip seperti perintah PLAY dimana pertama input string dipisah menggunakan `delimiter`, kemudian dicari ke dalam playlist.txt, untuk membandingkan nya menggunakan `strcasestr` agar tidak case sensitive

![image](https://cdn.discordapp.com/attachments/853191323589541908/1106930507410833458/image.png)
![image](https://cdn.discordapp.com/attachments/853191323589541908/1106930956100710444/image.png)
![image](https://cdn.discordapp.com/attachments/853191323589541908/1106931122903978074/image.png)
![image](https://cdn.discordapp.com/attachments/853191323589541908/1106931733141663864/image.png)
![image](https://cdn.discordapp.com/attachments/853191323589541908/1106932936386826300/image.png)
![image](https://cdn.discordapp.com/attachments/853191323589541908/1106933181480968213/image.png)
![image](https://cdn.discordapp.com/attachments/853191323589541908/1106933444371558490/image.png)

- Kesulitan nomer 3, parsing json dan decode json, penggunaan semaphore 


# Nomor 4
Soal Unzip.c

Download dan unzip file tersebut dalam kode c bernama unzip.c.

```bash
#define BUFFER_SIZE 1024

void *download(void *arg) {
    char *url = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
    char *file_name = "hehe.zip";

    char command[BUFFER_SIZE];
    sprintf(command, "curl -L -o %s %s", file_name, url);

    system(command);
    pthread_exit(NULL);
}
```
- Fungsi download() di atas digunakan untuk mengunduh file dari URL yang diberikan menggunakan perintah curl. Pertama, variabel url dan file_name didefinisikan sebagai string. 

- Variabel url berisi URL dari file yang ingin diunduh, sedangkan variabel file_name berisi nama file yang akan diunduh. 

- Kemudian, variabel command didefinisikan sebagai string dengan ukuran BUFFER_SIZE, di mana BUFFER_SIZE didefinisikan sebagai konstanta di awal program. 

- Variabel command akan berisi perintah untuk mengunduh file 
menggunakan curl.

```bash
void *extract(void *arg) {
    char *file_name = "hehe.zip";

    char command[BUFFER_SIZE];
    sprintf(command, "unzip %s", file_name);  
    
    system(command);
    pthread_exit(NULL);
}
```
- Fungsi extract() digunakan untuk mengekstraksi file yang telah diunduh menggunakan perintah unzip. 

- Nama file yang akan diekstraksi ditentukan dalam variabel file_name. 

- Kemudian, fungsi sprintf() digunakan untuk memformat string command, yang akan berisi perintah untuk mengekstraksi file dengan nama file_name. 

- Fungsi system() digunakan untuk mengeksekusi perintah tersebut pada terminal. 

- Setelah perintah dijalankan, fungsi pthread_exit() dipanggil untuk mengakhiri thread.

```bash
int main() {
    pthread_t download_thread, extract_thread;
    int status;

    status = pthread_create(&download_thread, NULL, download, NULL);
    if (status != 0) {
        printf("Thread Gagal\n");
        exit(EXIT_FAILURE);
    }
    
    status = pthread_create(&extract_thread, NULL, extract, NULL);
    if (status != 0) {
        printf("Thread Gagal\n");
        exit(EXIT_FAILURE);
    }

    pthread_join(download_thread, NULL);
    pthread_join(extract_thread, NULL);
    
    return 0;
}
```
- Fungsi main() pertama-tama membuat dua variabel bertipe pthread_t untuk masing-masing thread download dan extract. 

- Variabel status digunakan untuk menampung nilai pengembalian dari fungsi pthread_create() yang digunakan untuk 
membuat thread. 

- Jika nilai status bukan 0, maka thread tidak dapat dibuat dan program akan keluar dengan status Thread Gagal.
Terakhir, fungsi pthread_exit() dipanggil untuk mengakhiri thread. 

- Fungsi pthread_exit() digunakan untuk mengembalikan nilai NULL sebagai penanda bahwa 
thread telah selesai dijalankan.

- Kemudian, fungsi pthread_join() digunakan untuk menunggu thread download dan extract selesai dijalankan sebelum program berakhir. Fungsi pthread_join() memastikan bahwa program tidak berakhir sebelum kedua thread selesai 
dijalankan.


Soal Categorize.c

Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other. Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.

```bash
#define MAX_EXT_LEN 20
#define MAX_NUM_THREADS 10

void *create_folder(void *arg);
int is_directory(const char *path);
void create_categorized_folder();
void create_other_folder();

char extensions[MAX_NUM_THREADS][MAX_EXT_LEN]; // Array untuk menyimpan extensions
int num_threads = 0; // Jumlah threads yang akan dibuat
```
- Pertama, terdapat definisi beberapa konstanta yang akan digunakan pada program, yaitu MAX_EXT_LEN (panjang maksimal ekstensi file), dan MAX_NUM_THREADS (jumlah maksimal thread yang dibuat).

- Selanjutnya, terdapat deklarasi beberapa fungsi yang akan digunakan pada program, yaitu fungsi create_folder() dan is_directory(). Fungsi create_folder() akan digunakan untuk membuat folder, sedangkan fungsi is_directory() akan digunakan untuk mengecek apakah sebuah path merupakan sebuah direktori 
atau bukan.

- Kemudian, terdapat dua fungsi yaitu create_categorized_folder() dan create_other_folder(). Fungsi create_categorized_folder() akan membuat folder "categorized" dan membuat folder-folder di dalamnya sesuai dengan ekstensi file yang ada pada array extensions. 

- Sedangkan, fungsi create_other_folder() akan membuat folder "other" yang akandigunakan untuk menyimpan file-file yang tidak memiliki ekstensi yang terdaftar pada array extensions.

- Terakhir, terdapat variabel global yaitu array dua dimensi "extensions" dengan ukuran MAX_NUM_THREADS x MAX_EXT_LEN yang akan digunakan untuk menyimpan daftar ekstensi yang akan dicari pada file-file yang akan diproses, dan variabel "num_threads" yang akan digunakan untuk menyimpan jumlah thread yang akan dibuat.

```bash
int main() {
    FILE *fp;
    char extension[MAX_EXT_LEN];
    pthread_t threads[MAX_NUM_THREADS];

    // Buka extensions.txt file
    fp = fopen("extensions.txt", "r");
    if (fp == NULL) {
        printf("Error: Unable to open extensions.txt file\n");
        exit(1);
    }

    // Baca extensions dari file extensions.txt dan simpan dalam array
    while (fgets(extension, MAX_EXT_LEN, fp) != NULL && num_threads < MAX_NUM_THREADS) {
        extension[strcspn(extension, "\n")] = 0;
        strcpy(extensions[num_threads], extension);
        num_threads++;
    }
```
- Variabel fp bertipe FILE* dan akan digunakan untuk menyimpan alamat file yang sedang dibuka. Variabel extension bertipe char array dengan panjang MAX_EXT_LEN (20 karakter) dan akan digunakan untuk menyimpan ekstensi yang dibaca dari file "extensions.txt". Variabel threads bertipe pthread_t array dengan panjang 
MAX_NUM_THREADS (10 thread) dan akan digunakan untuk menyimpan thread yang akan dibuat.

- Program membuka file "extensions.txt" menggunakan fungsi fopen() dengan mode "r" (read). Jika file tidak berhasil dibuka, program akan menampilkan pesan kesalahan "Error: Unable to open extensions.txt file" dan keluar dari program menggunakan fungsi exit() dengan nilai 1.

- Kemudian membaca baris-baris di dalam file menggunakan fgets() dan menyimpan ekstensi yang dibaca ke dalam array extensions. Fungsi fgets() membaca baris dalam file hingga ditemukan karakter newline (\n) atau maksimal sebanyak MAX_EXT_LEN karakter

- Kemudian menghapus karakter newline dari string menggunakan strcspn(). Setelah itu, ekstensi yang telah dibaca disimpan dalam array extensions. Variabel num_threads digunakan untuk menghitung jumlah ekstensi yang telah dibaca dan dijadikan sebagai batas maksimal jumlah thread 
yang akan dibuat. Jika sudah mencapai batas maksimal, maka pembacaan file dihentikan.


```bash
    create_categorized_folder();
    create_other_folder();

    // Buat folder untuk setiap extension dengan threads
    for (int i = 0; i < num_threads; i++) {
        pthread_create(&threads[i], NULL, create_folder, extensions[i]);
    }

    // Join threads
    for (int i = 0; i < num_threads; i++) {
        pthread_join(threads[i], NULL);
    }

    return 0;
}
```
- Kode di atas memanggil dua fungsi yaitu create_categorized_folder() dan create_other_folder() untuk membuat folder "categorized" dan "other" di dalam direktori yang sama dengan program yang dijalankan. 

- Setelah itu, dilakukan iterasi sebanyak num_threads (jumlah ekstensi yang akan dicategorikan) untuk membuat thread sebanyak itu dengan memanggil fungsi pthread_create(). Thread-thread tersebut akan menjalankan fungsi 
create_folder() yang mengambil satu argumen yaitu string ekstensi yang akan dicategorikan. 

- Setelah semua thread selesai dijalankan, dilakukan join thread-thread tersebut dengan menggunakan perulangan for untuk memanggil fungsi pthread_join() pada tiap thread. Kemudian program selesai dengan mengembalikan nilai 0.

```bash
void *create_folder(void *arg) {
    char ext[MAX_EXT_LEN];
    sprintf(ext, "%s", (char*) arg);

    char folder_name[MAX_EXT_LEN];
    strcpy(folder_name, ext);

    // Mengubah nama file ke lowercase
    for (int i = 0; folder_name[i]; i++) {
        folder_name[i] = tolower(folder_name[i]);
    }

    // Membuat folder di folder categorized
    char folder_path[MAX_EXT_LEN + 11] = "categorized/";
    strcat(folder_path, folder_name);

    // Membuat folder
    if (!is_directory(folder_path)) {
        if (mkdir(folder_path, 0777) == -1) {
            printf("Error: Unable to create folder %s\n", folder_path);
            pthread_exit(NULL);
        }
    }

    pthread_exit(NULL);
}
```
- Fungsi create_folder adalah untuk membuat folder baru dalam direktori "categorized" berdasarkan ekstensi file yang diberikan sebagai argumen. Pada awal fungsi, ekstensi file disalin ke variabel ext menggunakan fungsi 
Sprintf() dan kemudian nama folder yang akan dibuat disimpan dalam variabel folder_name. 

- Nama folder ini dihasilkan dari ekstensi file yang telah diubah menjadi huruf kecil menggunakan fungsi tolower dan perulangan for.

- Setelah nama folder dibuat, variabel folder_path digunakan untuk menyimpan path lengkap menuju folder yang akan dibuat. Path tersebut terdiri dari direktori "categorized/" dan nama folder yang telah dibuat sebelumnya.

- Akhirnya, fungsi mkdir digunakan untuk membuat folder baru dengan path folder_path. Sebelum pembuatan folder, fungsi is_directory dipanggil untuk memeriksa apakah folder sudah ada atau belum. 

- Jika folder sudah ada, maka 
pembuatan folder akan dilewati. Jika pembuatan folder gagal, fungsi akan 
mencetak pesan kesalahan dan mengakhiri thread dengan pthread_exit(NULL).

```bash
// Berfungsi untuk memeriksa apakah path yang diberikan adalah direktori
int is_directory(const char *path) {
    struct stat path_stat;
    if (stat(path, &path_stat) != 0) {
        return 0;
    }
    return S_ISDIR(path_stat.st_mode);
}
```
- Fungsi is_directory adalah sebuah fungsi yang menerima sebuah string path yang merupakan path menuju sebuah direktori atau file pada sistem operasi. 

- Fungsi is_directory menggunakan struct stat yang menyediakan informasi tentang sebuah file atau direktori seperti ukuran, tipe file, waktu modifikasi, dan lain sebagainya. Struct stat didefinisikan di dalam library header sys/stat.h, kemudian fungsi stat akan dipanggil dengan dua parameter yaitu path dan &path_stat. Fungsi stat akan mengisi variabel path_stat dengan informasi tentang path yang diberikan. Fungsi stat mengembalikan nilai 0 jika berhasil dan -1 jika terdapat error.

- Fungsi is_directory memeriksa apakah path tersebut adalah direktori atau bukan dengan menggunakan fungsi S_ISDIR yang juga didefinisikan pada header file sys/stat.h. Jika S_ISDIR mengembalikan nilai 1, maka path tersebut adalah direktori.

- Jika tidak, maka path tersebut adalah file atau path yang tidak valid. Fungsi is_directory mengembalikan nilai 1 jika path yang diberikan adalah direktori dan 0 jika bukan direktori.

```bash
// Membuat categorized folder 
void create_categorized_folder() {
    if (!is_directory("categorized")) {
        if (mkdir("categorized", 0777) == -1) {
            printf("Error: Unable to create categorized folder\n");
            exit(1);
        }
    }
}
```
- Fungsi create_categorized_folder() digunakan untuk membuat folder dengan nama "categorized" jika folder tersebut belum ada di dalam direktori tempat program dijalankan.

- Fungsi akan memeriksa apakah folder "categorized" sudah ada atau belum dengan memanggil fungsi is_directory() yang akan memeriksa apakah path yang diberikan merupakan sebuah direktori atau bukan. 

- Jika folder tersebut belum ada, maka fungsi akan membuat folder tersebut dengan memanggil fungsi mkdir() dan 
memberikan permission dengan nilai 0777.

- Jika proses pembuatan folder gagal, maka program akan menampilkan pesan error 
dan keluar dari program menggunakan fungsi exit(1)

```bash
void create_other_folder() {
    char folder_path[21] = "categorized/other";
    // Buat other folder 
    if (!is_directory(folder_path)) {
        if (mkdir(folder_path, 0777) == -1) {
            printf("Error: Unable to create other folder\n");
            exit(1);
        }
    }
}
```
- Fungsi create_other_folder() berfungsi untuk membuat folder "other" di dalam folder "categorized" jika folder tersebut belum ada. Jika folder berhasil dibuat, maka fungsi akan berakhir. Namun, jika folder gagal dibuat, maka akan dicetak pesan kesalahan "Error: Unable to create other folder" dan program akan keluar.

- Pada kode char folder_path[21] = "categorized/other"; digunakan untuk menentukan path folder "other", yaitu "categorized/other". 

- Kemudian dilakukan pengecekan apakah folder tersebut sudah ada dengan menggunakan fungsi is_directory().

- Jika folder tersebut belum ada, maka fungsi akan membuat folder tersebut dengan memanggil fungsi mkdir() dan 
memberikan permission dengan nilai 0777.

- Jika proses pembuatan folder gagal, maka program akan menampilkan pesan kesalahan "Error: Unable to create other folder" dan program akan keluar.


Kesulitan dalam mengerjakan praktikum :
- Soal No 4 Categorize.c : Poin b-f
- Soal No 4 logchecker.c
-
-
-
-

