
// C Program for Message Queue (Reader Process)
//gcc -o stream stream.c -ljansson
#define _GNU_SOURCE
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <jansson.h>
#include <ctype.h>
#include <semaphore.h>

#define MAX_SENDERS 2
#define MAX_MSG_SIZE 240

sem_t mutex;

// structure for message queue
struct mesg_buffer {
    long mesg_type;
    char mesg_text[MAX_MSG_SIZE];

} message;



void download_file() {
    pid_t child_id;
    int status;
    char *url = "https://transfer.sh/get/tN3N7I/song-playlist.json";
    char *filename = "song-playlist.json";
    if((child_id = fork()) == 0){
    execlp("wget", "wget", "--no-check-certificate", url, "-O", filename, NULL);
    }
    while(wait(&status) > 0);
    printf("file downloaded\n");
   
}

void decode_rot13(const char* str, char* result) {
    int i;
    for (i = 0; str[i] != '\0'; ++i) {
        char c = str[i];
        if (c >= 'a' && c <= 'z') {
            result[i] = (((c - 'a') + 13) % 26) + 'a';
        } 
        
        else if (c >= 'A' && c <= 'Z') {
            result[i] = (((c - 'A') + 13) % 26) + 'A';
        } 
        
        else {
            result[i] = c;
        }
    }
    result[i] = '\0';
}

void decode_base64(const char* encrypt, char* decrypt) {
    const char b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    char b[4];
    int i = 0;
    int j = 0;
    
    while (encrypt[i]) {
        
        if (isspace(encrypt[i])) {
            i++;
            continue;
        }
        int k = 0;
        for (k = 0; k < 4 && encrypt[i]; k++, i++) {
            const char* pos = strchr(b64chars, encrypt[i]);
            if (pos == NULL) {
                decrypt[j] = '\0';
                return;
            }
            b[k] = pos - b64chars;
        }

        decrypt[j++] = (b[0] << 2) | (b[1] >> 4);

        if (k > 2) {
            decrypt[j++] = (b[1] << 4) | (b[2] >> 2);
        }
        if (k > 3) {
            decrypt[j++] = (b[2] << 6) | b[3];
        }
    }
    
    decrypt[j] = '\0';
}



void decode_hex(const char* input, char* output) {
    size_t len = strlen(input);
    if (len % 2 != 0) {
        // Invalid input length
        return;
    }

    size_t output_len = len / 2;
    char temp[3];
    temp[2] = '\0';

    for (size_t i = 0; i < output_len; i++) {
        temp[0] = tolower(input[i * 2]);
        temp[1] = tolower(input[i * 2 + 1]);

        int value = 0;
        for (size_t j = 0; j < 2; j++) {
            if (temp[j] >= '0' && temp[j] <= '9') {
                value = (value << 4) + (temp[j] - '0');
            } else if (temp[j] >= 'a' && temp[j] <= 'f') {
                value = (value << 4) + (temp[j] - 'a' + 10);
            } else {
                // Invalid input character
                return;
            }
        }
        output[i] = (char) value;
    }
    output[output_len] = '\0';
}


int main()
{
    key_t key;
    int msgid, i;
    char senders[MAX_SENDERS][MAX_MSG_SIZE] = {0};
    int sender_count = 0;
    char file_name[] = "song-playlist.json";
    char file_name_output[] = "playlist.txt";
    

    // ftok to generate unique key
    key = ftok("progfile", 65);

    // msgget creates a message queue
    // and returns identifier
    msgid = msgget(key, 0666 | IPC_CREAT);

    sem_init(&mutex, 0, 2);

    while (1) {

        sem_post(&mutex);
        // check for messages
        if (msgrcv(msgid, &message, sizeof(message), 0, IPC_NOWAIT) != -1) {

            // check if sender is already registered
            int found = 0;
            for (i = 0; i < sender_count; i++) {
                if (message.mesg_type == atol(senders[i])) {
                    found = 1;
                    // printf("system overload\n");
                    break;
                }
            }
 
            // if not, add sender to list
            if (!found) {
                sprintf(senders[sender_count], "%ld", message.mesg_type);
                sender_count++;
            }

        
         sem_post(&mutex);

    printf("\nReceived message %s from user %ld\n\n", message.mesg_text, message.mesg_type);
    
    if (strstr(message.mesg_text, "PLAY") == NULL && 
    strstr(message.mesg_text, "BREAK") == NULL && 
    strstr(message.mesg_text, "LIST") == NULL && 
    strstr(message.mesg_text, "ADD") == NULL) {
    printf("UNKNOWN COMMAND\n");
    }


    if (strstr(message.mesg_text, "DECRYPT") != NULL){
            
                printf("Decrypting...\n");
                download_file();

                // printf("opening json file\n");

                FILE *input_file = fopen(file_name, "r");

                    if (!input_file) {
                        printf("Error: cannot open  %s\n", file_name);
                    }

                FILE *output_file = fopen(file_name_output, "w");
                    if (!output_file) {
                        printf("Error: cannot open  %s\n", file_name_output);
    
                }

                FILE *fp = fopen(file_name, "rb");
                if (!input_file) {
                        printf("Error: cannot open  %s\n", file_name);
                    }
                // printf("opening json done file\n");

             
                fseek(fp, 0L, SEEK_END);
                long fileSize = ftell(fp);
                rewind(fp);

                // printf("read\n");

                char* buffer = (char*)calloc(fileSize + 1, sizeof(char));

                // printf("read content\n");
                size_t bytesRead = fread(buffer, 1, fileSize, fp);
                buffer[bytesRead] = '\0';

                fclose(fp);
                json_error_t error;
                json_t *root = json_loads(buffer, 0, &error);
                free(buffer);

                // printf("parsing json file\n");

                
                fp = fopen(file_name_output, "w");
                if (!output_file) {
                        printf("Error: cannot open  %s\n", file_name_output);
                }

                size_t index;
                json_t *value;

                json_array_foreach(root, index, value) {
                    
                    json_t *method = json_object_get(value, "method");
                    json_t *song = json_object_get(value, "song");

                    const char *methodstring = json_string_value(method);
                    const char *songstring = json_string_value(song);

                    // Decrypt json file
                    char file_decrypt[MAX_MSG_SIZE];
                    
                    if (strcmp(methodstring, "hex") == 0) {
                        decode_hex(songstring, file_decrypt);
                        if(songstring != NULL){
                            fprintf(fp, "%s\n", file_decrypt);
                            
                        }                    
                    } else if (strcmp(methodstring, "base64") == 0) {
                        decode_base64(songstring, file_decrypt);
                        if(songstring != NULL){
                            fprintf(fp, "%s\n", file_decrypt);
                            
                        }
                    } else if (strcmp(methodstring, "rot13") == 0) {
                        decode_rot13(songstring, file_decrypt);
                        if(songstring != NULL){
                            fprintf(fp, "%s\n", file_decrypt);
                        
                        }  
                    } else {
                        printf("method is wrong %s\n", methodstring);
                         break;
                    }                    
                }
                json_decref(root);
                fclose(fp);

                system("sort  playlist.txt > sorted_playlist.txt");
                system("sort  sorted_playlist.txt > playlist.txt ");
                system("rm sorted_playlist.txt");
                }


   if (strstr(message.mesg_text, "LIST") != NULL){
            
                system("cat playlist.txt | sort ");
               // break;
        }
    if (strstr(message.mesg_text, "PLAY") != NULL){

                char delim[] = "\"";
                char *token;

                token = strtok(message.mesg_text, delim);

                // cari 
                token = strtok(NULL, delim);

            char line[1000];
            int count = 0;

            FILE *fp = fopen(file_name_output, "r");

            while (fgets(line, 1000, fp) != NULL) {
                if (strcasestr(line, token) != NULL) {
                count++;
                }
            }

            if (count == 1) {
            printf("USER %ld PLAYING \"%s\" \n", message.mesg_type, token);

            } else if (count > 1) {
                
                printf("THERE ARE %d SONG CONTAINING  \"%s\" \n",count, token );
            
                fclose(fp);
                count = 0;
                fp = fopen(file_name_output, "r");


                while (fgets(line, 100, fp) != NULL) {
                if (strcasestr(line, token) != NULL) {
                    count ++;

                printf("%d. %s", count, line);
                }
            }
            } else {
                printf("  THERE IS NO SONG CONTAINING \"%s\" \n", token);
            }

            fclose(fp);

            }


            if (strstr(message.mesg_text, "ADD") != NULL){
            
                char delim[] = "\"";
                char *token;

                token = strtok(message.mesg_text, delim);

                // cari 
                token = strtok(NULL, delim);

            char line[1000];
            int countAdd = 0;

            FILE *fp = fopen(file_name_output, "r");

            while (fgets(line, 1000, fp) != NULL) {
                if (strcasestr(line, token) != NULL) {
                countAdd++;
                }
                
            }

            fclose(fp);

            if (countAdd == 0 ) {
                FILE *fp = fopen(file_name_output, "a");
                fprintf(fp, "%s\n", token);
                fclose(fp);

                printf("USER %ld ADD \"%s\" \n", message.mesg_type, token);
            } else if (countAdd > 0){
                printf("SONG ALREADY ON PLAYLIST\n");
            }

        }
            // printf("Received message %s from sender %ld\n", message.mesg_text, message.mesg_type);
        }

        // sleep(1);
        }

    sem_destroy(&mutex);

          return 0;
}
