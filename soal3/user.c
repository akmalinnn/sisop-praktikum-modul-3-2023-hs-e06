// C Program for Message Queue (Writer Process)
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100

// structure for message queue
struct mesg_buffer {
    long mesg_type;
    char mesg_text[MAX];
} message;

int main()
{
    key_t key;
    int msgid;
    pid_t pid = getpid();

    // ftok to generate unique key
    key = ftok("progfile", 65);

    // msgget creates a message queue
    // and returns identifier
    msgid = msgget(key, 0666 | IPC_CREAT);

while(1){
    // generate a message type based on the process ID
    message.mesg_type = pid;

    printf("USER PID: %d\n", pid);
    printf("Enter message: ");
    fgets(message.mesg_text, MAX, stdin);

    // msgsnd to send message
    msgsnd(msgid, &message, sizeof(message), 0);

    // display the message
    printf("Data sent is : %s\n", message.mesg_text);

    if (strcmp(message.mesg_text, "END\n") == 0) {
            break;
    }
}
    return 0;
}

