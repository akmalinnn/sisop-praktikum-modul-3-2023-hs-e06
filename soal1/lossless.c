#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>


#define BUFFER_SIZE 256

struct node {
    char ch;
    int freq;
    struct node *left, *right;
};

struct node *new_node(char ch, int freq) {
    struct node *new_node = (struct node*) malloc(sizeof(struct node));
    new_node->ch = ch;
    new_node->freq = freq;
    new_node->left = NULL;
    new_node->right = NULL;
    return new_node;
}

void swap(struct node **a, struct node **b) {
    struct node *temp = *a;
    *a = *b;
    *b = temp;
}

void min_heapify(struct node **arr, int i, int heap_size) {
    int l = 2*i + 1;
    int r = 2*i + 2;
    int smallest = i;
    if (l < heap_size && arr[l]->freq < arr[smallest]->freq) {
        smallest = l;
    }
    if (r < heap_size && arr[r]->freq < arr[smallest]->freq) {
        smallest = r;
    }
    if (smallest != i) {
        swap(&arr[i], &arr[smallest]);
        min_heapify(arr, smallest, heap_size);
    }
}

struct node *build_min_heap(struct node **arr, int n) {
    int i;
    for (i = n/2-1; i >= 0; i--) {
        min_heapify(arr, i, n);
    }
    return arr[0];
}

void insert(struct node **arr, struct node *node, int *n) {
    (*n)++;
    int i = (*n) - 1;
    while (i && node->freq < arr[(i-1)/2]->freq) {
        arr[i] = arr[(i-1)/2];
        i = (i-1)/2;
    }
    arr[i] = node;
}

struct node *huffman_tree(char *str, int n) {
    struct node **arr = (struct node**) malloc(n * sizeof(struct node*));
    int i, j, k = 0;
    for (i = 0; i < n; i++) {
        int freq = 1;
        for (j = i+1; j < n; j++) {
            if (str[j] == str[i]) {
                freq++;
            } else {
                break;
            }
        }
        struct node *node = new_node(str[i], freq);
        insert(arr, node, &k);
        i = j-1;
    }
    while (k > 1) {
        struct node *left = build_min_heap(arr, k);
        swap(&arr[0], &arr[k-1]);
        k--;
        struct node *right = build_min_heap(arr, k);
        swap(&arr[0], &arr[k-1]);
        k--;
        struct node *node = new_node('$', left->freq + right->freq);
        node->left = left;
        node->right = right;
        insert(arr, node, &k);
    }
    struct node *huffman_tree = build_min_heap(arr, k);
    free(arr);
    return huffman_tree;
}

void print_codes(struct node *root, int arr[], int top, int fd_out) {
    if (root->left) {
        arr[top] = 0;
         print_codes(root->left, arr, top+1, fd_out);
    }
    if (root->right) {
        arr[top] = 1;
        print_codes(root->right, arr, top+1, fd_out);
    }
    if (!root->left && !root->right) {
        dprintf(fd_out, "%c:", root->ch);
        int i;
        for (i = 0; i < top; i++) {
            dprintf(fd_out, "%d", arr[i]);
        }
        dprintf(fd_out, "\n");
    }
}

void *compress(void *arg) {
    char *filename = (char *) arg;
    char out_filename[200];
    sprintf(out_filename, "%s.huf.txt", filename);

int fd_in = open(filename, O_RDONLY);
if (fd_in == -1) {
    perror("open");
    pthread_exit(NULL);
}

int fd_out = open(out_filename, O_CREAT | O_WRONLY | O_TRUNC, 0644);
if (fd_out == -1) {
    perror("open");
    close(fd_in);
    pthread_exit(NULL);
}

char buffer[BUFFER_SIZE];
int n_read;
while ((n_read = read(fd_in, buffer, BUFFER_SIZE)) > 0) {
    int i;
    for (i = 0; i < n_read; i++) {
        // TODO: implement huffman encoding
    }
    write(fd_out, buffer, n_read);
}

close(fd_in);
close(fd_out);
pthread_exit(NULL);
}

int main() {
int fd[2];
int fd2[2];
pid_t pid;

if (pipe(fd) == -1) {
    perror("pipe");
    return EXIT_FAILURE;
}

if (pipe(fd2) == -1) {
    perror("pipe");
    return EXIT_FAILURE;
}

pid = fork();
if (pid == -1) {
    perror("fork");
    return EXIT_FAILURE;
}

if (pid == 0) {
    // Child process
    close(fd[1]);
    close(fd2[0]);

    // TODO: receive frequency count from parent process
    // then use it to build huffman tree and encode the file

    pthread_t tid;
    pthread_create(&tid, NULL, compress, "file.txt");
    pthread_join(tid, NULL);

    close(fd[0]);
    close(fd2[1]);
    exit(EXIT_SUCCESS);
} else {
    // Parent process
    close(fd[0]);
    close(fd2[1]);

    char buffer[BUFFER_SIZE];
    int n_read;
    while ((n_read = read(STDIN_FILENO, buffer, BUFFER_SIZE)) > 0) {
        write(fd[1], buffer, n_read);
    }
    close(fd[1]);

    // TODO: read frequency count from child process
    // then print the huffman code for each character to stdout

    close(fd[1]);
    close(fd2[0]);
    wait(NULL);
    exit(EXIT_SUCCESS);
}

}
