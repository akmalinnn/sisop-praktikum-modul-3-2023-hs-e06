#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <pthread.h>

#define BUFFER_SIZE 1024

void *download(void *arg) {
    char *url = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
    char *file_name = "hehe.zip";

    char command[BUFFER_SIZE];
    sprintf(command, "curl -L -o %s %s", file_name, url);

    system(command);
    pthread_exit(NULL);
}

void *extract(void *arg) {
    char *file_name = "hehe.zip";

    char command[BUFFER_SIZE];
    sprintf(command, "unzip %s", file_name);  
    
    system(command);
    pthread_exit(NULL);
}

int main() {
    pthread_t download_thread, extract_thread;
    int status;

    status = pthread_create(&download_thread, NULL, download, NULL);
    if (status != 0) {
        printf("Thread Gagal\n");
        exit(EXIT_FAILURE);
    }
    
    status = pthread_create(&extract_thread, NULL, extract, NULL);
    if (status != 0) {
        printf("Thread Gagal\n");
        exit(EXIT_FAILURE);
    }

    pthread_join(download_thread, NULL);
    pthread_join(extract_thread, NULL);
    
    return 0;
}
