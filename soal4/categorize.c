#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <ctype.h>
#include <pthread.h>
#include <sys/stat.h>

#define MAX_EXT_LEN 20
#define MAX_NUM_THREADS 10

void *create_folder(void *arg);
int is_directory(const char *path);
void create_categorized_folder();
void create_other_folder();

char extensions[MAX_NUM_THREADS][MAX_EXT_LEN]; // Array untuk menyimpan extensions
int num_threads = 0; // Jumlah threads yang akan dibuat

int main() {
    FILE *fp;
    char extension[MAX_EXT_LEN];
    pthread_t threads[MAX_NUM_THREADS];

    // Buka extensions.txt file
    fp = fopen("extensions.txt", "r");
    if (fp == NULL) {
        printf("Error: Unable to open extensions.txt file\n");
        exit(1);
    }

    // Baca eextensions dari file extensions.txt dan simpan dalam array
    while (fgets(extension, MAX_EXT_LEN, fp) != NULL && num_threads < MAX_NUM_THREADS) {
        extension[strcspn(extension, "\n")] = 0;
        strcpy(extensions[num_threads], extension);
        num_threads++;
    }

    create_categorized_folder();
    create_other_folder();

    // Buat folder untuk setiap extension dengan threads
    for (int i = 0; i < num_threads; i++) {
        pthread_create(&threads[i], NULL, create_folder, extensions[i]);
    }

    // Join threads
    for (int i = 0; i < num_threads; i++) {
        pthread_join(threads[i], NULL);
    }

    return 0;
}

void *create_folder(void *arg) {
    char ext[MAX_EXT_LEN];
    sprintf(ext, "%s", (char*) arg);

    char folder_name[MAX_EXT_LEN];
    strcpy(folder_name, ext);

    // Mengubah nama file ke lowercase
    for (int i = 0; folder_name[i]; i++) {
        folder_name[i] = tolower(folder_name[i]);
    }

    // Membuat folder di folder categorized
    char folder_path[MAX_EXT_LEN + 11] = "categorized/";
    strcat(folder_path, folder_name);

    // Membuat folder
    if (!is_directory(folder_path)) {
        if (mkdir(folder_path, 0777) == -1) {
            printf("Error: Unable to create folder %s\n", folder_path);
            pthread_exit(NULL);
        }
    }

    pthread_exit(NULL);
}

// Berfungsi untuk memeriksa apakah path yang diberikan adalah direktori
int is_directory(const char *path) {
    struct stat path_stat;
    if (stat(path, &path_stat) != 0) {
        return 0;
    }
    return S_ISDIR(path_stat.st_mode);
}

// Membuat categorized folder 
void create_categorized_folder() {
    if (!is_directory("categorized")) {
        if (mkdir("categorized", 0777) == -1) {
            printf("Error: Unable to create categorized folder\n");
            exit(1);
        }
    }
}

void create_other_folder() {
    char folder_path[21] = "categorized/other";
    // Buat other folder 
    if (!is_directory(folder_path)) {
        if (mkdir(folder_path, 0777) == -1) {
            printf("Error: Unable to create other folder\n");
            exit(1);
        }
    }
}
