#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

int main(){
    int shmid;
    key_t key = 1234;
    int (*matrix)[5];

    int matriks1[4][2];
    int matriks2[2][5];
    int hasil[4][5] = {0};
    srand(time(NULL));

    int i, j, k;
    //inisialisasi matriks 1
    for(i = 0; i<4; i++){
        for(j = 0; j<2; j++){
            matriks1[i][j] = rand()%5+1; //diisi angka random 1-5
        }
    }

    //inisialisasi matriks 2
    for(i=0; i<2; i++){
        for(j=0; j<5; j++){
            matriks2[i][j] = rand()%4+1; //diisi angka random 1-4
        }
    }

    //mengalikan matriks
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            for(k=0; k<2; k++){
                hasil[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }

    //menampilkan hasil
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    // membuat segment shared memory
    if ((shmid = shmget(key, sizeof(int), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    // menambahkan ke segmen shared memory
    if ((matrix = shmat(shmid, NULL, 0)) == (int (*)[5]) -1) {
        perror("shmat");
        exit(1);
    }

    // menyalin hasil ke shared memory
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            matrix[i][j] = hasil[i][j];
        }
    }

    // melepaskan dari shared memory
    shmdt(matrix);

    return 0;

}
