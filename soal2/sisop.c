#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

unsigned long long faktorial(int n){ 
    unsigned long long res = 1;
    int i;
    for(i=1; i<=n; i++){
        res *= i;
    }
    return res;
}

int main(){
    
    int shmid;
    key_t key = 1234;
    int (*matrix)[5];

    int hasil[4][5];
    unsigned long long fakt[4][5];

    int i, j, temp;

    clock_t start = clock();

    if ((shmid = shmget(key, sizeof(int), 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    if ((matrix = shmat(shmid, NULL, 0)) == (int (*)[5]) -1) {
        perror("shmat");
        exit(1);
    }

    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            hasil[i][j] = matrix[i][j];
        }
    }

    shmdt(matrix);


    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            temp = hasil[i][j];
            fakt[i][j] = faktorial(temp);
        }
    }

    clock_t end = clock();

    printf("matriks perkalian:\n");
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            printf("%d ", hasil[i][j]);
        }
        printf("\n");
    }

    printf("faktorial matriks:\n");
    for(i=0; i<4; i++){
        for(j=0; j<5; j++){
            printf("%llu ", fakt[i][j]);
        }
        printf("\n");
    }

    double waktu = ((double)(end - start)) / CLOCKS_PER_SEC;
    printf("Waktu eksekusi program : %f detik", waktu);

}
